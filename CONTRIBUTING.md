# Contributing

## Making Changes
1. Make the code changes
2. Test the code changes - see below
3. Update the version: pipe.yml
4. Update changelog.md

## Testing
1. Run unit tests: `Invoke-Pester`
2. Build using: `docker build . -t test`
3. Invoke the container for integration testing with Bitbucket.  See test commands below.

### Basic Testing
2. Invoke container: `docker run -e "DEBUG=true" -e "BITBUCKET_WORKSPACE=name" -e "REPO=name" -e "BB_AUTH=username:token" -it test`

### Custom Run Testing
2. Invoke container: `docker run -e "DEBUG=true" -e "BITBUCKET_WORKSPACE=name" -e "CUSTOM=fail" -e "REPO=name" -e "BB_AUTH=username:token" -it test`

### Custom Run and Variables
2. Invoke container: `docker run -e "DEBUG=true" -e "BITBUCKET_WORKSPACE=name" -e "CUSTOM=fail" -e "VAR_PASSED=Test_Value" -e "SEC_VAR_SENSITIVE=PASSWORD" -e "REPO=name" -e "BB_AUTH=username:token" -it test`