using module .\helpers.psm1
using module .\wait-preexistingpipeline.psm1


<#
  .SYNOPSIS
    Starts a pipelne

  .DESCRIPTION
    Starts a pipeline, verifies completion, and retries if paused or halted
#>
function Invoke-Pipeline {

  [CmdletBinding()]
  param (
    $Workspace,
    $Repo,
    $Branch = 'master',
    $Confirm = 'true',
    $Custom,
    [int] $restartLimit = 5,
    [int] $restartTimeout = 3600,
    [array]$Variables
  )

  $counter = 0

  # Initiate loop
  do {
    $counter++

    # Start new pipeline
    Write-BBBlue 'Starting pipeline'
    $pipeline = Start-BitbucketPipeline -Workspace $Workspace -RepoSlug $Repo -CustomPipe $Custom -Branch $Branch -Variables $Variables -ErrorAction 'Stop'
    Write-BBBlue "Pipeline Started: https://bitbucket.org/$Workspace/$Repo/addon/pipelines/home#!/results/$($pipeline.build_number)"

    # Watch new pipeline for successful completion
    if ($Confirm -eq 'true') {
      try {
        Write-BBBlue "Watching pipeline https://bitbucket.org/$Workspace/$Repo/addon/pipelines/home#!/results/$($pipeline.build_number)"
        $response = Wait-BitbucketPipeline -Workspace $Workspace -RepoSlug $Repo -PipelineUUID $pipeline.uuid

        # Exit the loop
        Break

      }
      catch {

        # If the new pipeline is halted, watch the currently running pipeline until it is finished
        if ($_ -like "*halted*" -or $_ -like "*pending*") {

          # If the pipeline was halted on the final retry, don't attempt to wait for the preexisting pipeline to finish
          if ($counter -le $restartLimit) {
            Write-BBWarning "Pipeline halted! `nAttempting recovery (attempt $counter of $restartLimit)"
            Wait-PreexistingPipeline -Workspace $Workspace  -Repo $Repo -PipelineUUID $pipeline.uuid -restartTimeout $restartTimeout
            Write-BBBlue 'Pre-existing running pipeline finished'
          }
        }
        else {

          # A different error happened
          Throw $_
        }
      }
    }
    # Continue restarting until the limit has been reached.  If RESTART_LIMIT has been set to 0 no restart attempts are made.
  } while ($counter -le $restartLimit -and $Confirm -eq 'true')
  Return $response
}

Export-ModuleMember Invoke-Pipeline