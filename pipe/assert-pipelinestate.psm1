using module .\helpers.psm1

<#
  .SYNOPSIS
    Returns pipeline run status output

  .DESCRIPTION
    Returns the completed status of a pipeline along with helpful information.
#>
function Assert-PipelineState {
  Param (
    [string] $Workspace,
    [string] $Repo,
    [object] $PipelineData
  )
  $triggerURL = "https://bitbucket.org/$Workspace/$Repo/addon/pipelines/home#!/results/$($PipelineData.build_number)"

  switch ($PipelineData.state.result.name) {
    'SUCCESSFUL' {
      Write-BBSuccess "The triggered pipeline was succesfully completed!"
      Write-BBSuccess $triggerURL
    }
    'STOPPED' {
      Write-BBTerminate "The triggered pipeline was stopped by: $($PipelineData.state.result.terminator.display_name).  See the pipeline for more details: $triggerURL"
    }
    'FAILED' {
      Write-BBTerminate "The triggered pipeline has failed.  See the pipeline for more details: $triggerURL"
    }
    Default {
      Write-BBTerminate "The pipeline ended in an unknown state of: $($PipelineData.state.result.name).  See the pipeline for more details: $triggerURL"
    }
  }
}

Export-ModuleMember Assert-PipelineState