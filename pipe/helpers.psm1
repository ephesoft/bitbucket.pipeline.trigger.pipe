# ANSI Escape Codes for coloring Bitbucket Pipelines Output
$ESC = [char]27
$reset = "$ESC[0m"

function Write-BBSuccess ($string) {
    Write-Host "$ESC[32m$string$reset"
}

function Write-BBWarning ($string) {
    Write-Host "$ESC[33m$string$reset"
}

function Write-BBBlue ($string) {
    Write-Host "$ESC[36m$string$reset"
}

function Write-BBGray ($string) {
    Write-Host "$ESC[37m$string$reset"
}

function Write-BBError ($string) {
    Write-Host "$ESC[31m$string$reset"
}

function Write-BBTerminate ($string) {
    Write-Host "$ESC[31m$string$reset"
    Exit 1
}