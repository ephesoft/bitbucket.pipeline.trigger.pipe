<#
  .SYNOPSIS
    Returns variables from Bitbucket

  .DESCRIPTION
    Returns variables from Bitbucket
#>
function Get-BBPVariable {
    [CmdletBinding()]
    param (
    )

    # Create empty array for storage of variables
    $variables = @()

    # Add non-secured variables to array
    $var = Get-ChildItem ENV:VAR_*
    foreach ($_var in $var) {
        $variables += @{
            key = $_var.name.replace('VAR_','')
            value = $_var.value
        }
    }

    # Add secured variables to array
    $sec_var = Get-ChildItem ENV:SEC_VAR_*
    foreach ($_sec_var in $sec_var) {
        $variables += @{
            key = $_sec_var.name.replace('SEC_VAR_','')
            value = $_sec_var.value
            secured = true
        }
    }

    return $variables
}

Export-ModuleMember Get-BBPVariable