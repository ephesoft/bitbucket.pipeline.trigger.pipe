using module .\helpers.psm1

<#
  .SYNOPSIS
    Waits for a pre-existing pipeline to complete

  .DESCRIPTION
    Waits for a pre-existing pipeline to complete
#>
function Wait-PreexistingPipeline {
  Param (
    [string] $Workspace,
    [string] $Repo,
    [string] $PipelineUUID,
    [int] $restartTimeout = 3600
  )
    # Get pipeline steps
    $steps = Get-BitbucketPipelineStep -WorkSpace $Workspace -Repo $Repo -PipelineUUID $PipelineUUID
    $haltedStep = $steps | Where-Object {$_.state.name -in ('PENDING', 'IN_PROGRESS') -and $_.state.stage.name -in ('PAUSED', 'HALTED')}
    $originalPipelineUUID = $haltedStep.state.stage.reason.pipeline_.uuid
    Write-BBBlue "Running pipeline UUID: $originalPipelineUUID"

    # Watch original pipeline
    Write-BBBlue 'Waiting for pre-existing running pipeline to finish'
    Wait-BitbucketPipeline -WorkSpace $Workspace -RepoSlug $Repo -PipelineUUID $originalPipelineUUID -TimeoutSeconds $restartTimeout | Out-Null
}

Export-ModuleMember Wait-PreexistingPipeline