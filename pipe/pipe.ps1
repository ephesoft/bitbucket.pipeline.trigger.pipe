using module .\assert-pipelinestate.psm1
using module .\get-bbpvariable.psm1
using module .\helpers.psm1
using module .\invoke-pipeline.psm1

if($ENV:DEBUG){
    Write-BBWarning 'Getting all Environment Variables for debugging'
    Get-ChildItem ENV:
    $VerbosePreference = "Continue"
}

# Check required environment variables and terminate if missing
if([string]::IsNullOrWhiteSpace($ENV:REPO)){
    Write-BBTerminate "Missing Variable: REPO"
}elseif([string]::IsNullOrWhiteSpace($ENV:BB_AUTH)){
    Write-BBTerminate "Missing Variable: BB_AUTH"
}elseif([string]::IsNullOrWhiteSpace($ENV:BITBUCKET_WORKSPACE)){
    Write-BBTerminate "Missing Variable: BITBUCKET_WORKSPACE.  This is provided automatically when running a pipe in Bitbucket.  If you are running locally for testing, please provide the variable."
}

# Check environment variables and add defaults if missing
if([string]::IsNullOrWhiteSpace($ENV:BRANCH)){
    Write-BBBlue 'No Branch specified using master'
    $ENV:BRANCH = 'master'
}
if([string]::IsNullOrWhiteSpace($ENV:RESTART_LIMIT)){
  Write-BBBlue 'No restart limit specified using default of 5'
  $ENV:RESTART_LIMIT = 5
}
if([string]::IsNullOrWhiteSpace($ENV:RESTART_TIMEOUT)){
  Write-BBBlue 'No restart timeout specified using default of 3600 seconds'
  $ENV:RESTART_TIMEOUT = 3600
}
if([string]::IsNullOrWhiteSpace($ENV:CONFIRM)){
  Write-BBBlue 'Confirm not specified using default true'
  $ENV:CONFIRM = 'true'
}

# Authenticate
$bbAuth = $ENV:BB_AUTH
$username = $bbAuth.substring(0,$bbAuth.indexof(':'))
$password = $bbAuth.substring(($bbAuth.indexof(':') +1)) | ConvertTo-SecureString -AsPlainText -Force
$cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $username, $password
New-BitbucketLogin -Credential $cred -Workspace $ENV:BITBUCKET_WORKSPACE

# Get Basic and secure variables
$Variables = Get-BBPVariable

$response = Invoke-Pipeline -Workspace $ENV:BITBUCKET_WORKSPACE -Repo $ENV:REPO -Branch $ENV:BRANCH -Custom $ENV:CUSTOM -Variables $Variables -restartLimit $ENV:RESTART_LIMIT -restartTimeout $ENV:RESTART_TIMEOUT -Confirm $ENV:CONFIRM

if ($ENV:CONFIRM -eq 'true') {
  Assert-PipelineState -Workspace $ENV:WORKSPACE -Repo $ENV:REPO -PipelineData $response
}
