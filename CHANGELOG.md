# Changelog

## 1.2.3 (2022-08-01)
- Fixed: added missing license file for public code (published to docker)

## 1.2.2 (2022-05-23)
- Fixed CONFIRM parameter being ignored when set to false

## 1.2.1 (2021-10-18)
- Fixed missed output when pipeline is first triggered

## 1.2.0 (2021-09-13)
- Updated pipe to handle paused/halted pipelines and retry once existing pipeline finishes
- Updated to Pester 5 support for unit tests
- Added new unit tests
- Added new optional parameters RESTART_LIMIT and RESTART_TIMEOUT

## 1.1.1
- Updated pipe to handle paused or halted builds
- Adding default pipeline values

## 1.1.0
- Updated to use pipe for publishing

## 1.0.1
- Fixed examples in readme.md

## 1.0.0
- Initial Release

## 0.x.x
- Testing and validation of POC.  Do not use 0.x versions, first release will be 1.x

## 0.1.0
- Testing POC