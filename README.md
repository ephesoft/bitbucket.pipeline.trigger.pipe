# Bitbucket Pipelines Pipe: Bitbucket Pipeline Trigger

Pipe to allow simple triggering of pipelines in other repos.

## Semantic Versioning
This pipe utilizes semantic versioning to allow the consumer to automatically pull in bug fixes, feature enhancements as well as major changes if they wish.  The recommended approach is to pull in all feature enhancements and manually update to new major releases.  To see the changes for each release review the **CHANGELOG.md** file.

### How to use Semantic Versioning in the YAML file
To use semantic versioning in the YAML file just specify the appropriate tag on the pipe.

### Tags
- All Versions: `latest` or no tag
- Major Version: `x`
- Minor Version: `x.x`
- Bug fix Version: `x.x.x`

### Examples
- **Recommended** - Pull in the latest version 1 pipe: `pipe: docker://ephesoft/bitbucket.pipeline.trigger:1`
- Pull in the latest version 1.2 pipe: `pipe: docker://ephesoft/bitbucket.pipeline.trigger:1.2`
- Pull in exactly version 1.2.3 pipe: `pipe: docker://ephesoft/bitbucket.pipeline.trigger:1.2.3`
- Pull in the latest pipe including new major versions: `pipe: docker://ephesoft/bitbucket.pipeline.trigger`

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

### Basic Usage
This will trigger the default pipeline against the master branch of a repo called `Repo1`.

```yaml
script:
  - pipe: docker://ephesoft/bitbucket.pipeline.trigger:1
    variables:
      REPO: "Repo1"
      BB_AUTH: $AUTH
```

### Custom Pipeline
This will trigger a custom pipeline called `Test` against the master branch of repo: `Repo1`.

```yaml
script:
  - pipe: docker://ephesoft/bitbucket.pipeline.trigger:1
    variables:
      REPO: "Repo1"
      BB_AUTH: $AUTH
      CUSTOM: "Test"
```

### Custom Pipeline with variables
This will trigger a custom pipeline called `Test` against the `SmokeTest` branch and pass in `API_URL` and `API_KEY` variables for the repo: `Repo1`.  The `API_URL` is hardcoded in this example and the `API_KEY` is stored in a secure Bitbucket variable on the repo or account.

```yaml
script:
  - pipe: docker://ephesoft/bitbucket.pipeline.trigger:1
    variables:
      REPO: "Repo1"
      BB_AUTH: $AUTH
      BRANCH: "SmokeTest"
      CUSTOM: "Test"
      VAR_API_URL: "https://domain.tld/endpoint"
      SEC_VAR_API_KEY: $API_KEY
```


## Variables
Variables are case sensitive.

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| REPO (*)              | Name of the Repo to trigger                                 |
| BB_AUTH (*)           | Authentication string in the form of <username:token>       |
| BRANCH                | Name of the Branch to trigger on. Default: `master`.        |
| CUSTOM                | Name of the Custom pipelines to trigger. Default: Non Custom Run.|
| VAR_***               | Variable to pass to the pipeline run.  The name used is the part after VAR_ |
| SEC_VAR_**            | Secure Variable to pass to the pipeline run. The name used is the part after SEC_VAR_ |
| CONFIRM               | Watch the triggered pipeline for succesfull completion and fail if not. Default: `true`.|
| DEBUG                 | Turn on extra debug information. Default: `false`.          |
| RESTART_LIMIT           | Number of times to try starting pipeline if pipeline gets paused or halted.  Default `5`.  Set to `0` to disable restart attempts |
| RESTART_TIMEOUT         | Number of seconds to poll for pre-existing running pipelines to finish before timing out.  Defaults to `3600`. |
_(*) = required variable._

### Using Custom Variables
You can pass custom variables to the pipeline that you are triggering.  This can be used to make your triggered pipeline dynamic at runtime.  The syntax allows you to pass any number of secure and non-secure variables to the receiving pipeline.

#### Regular Variables
To specify a regular variable to be picked up and passed to the pipeline just prefix your variable with VAR_.  For example the variable `API_URL` can be passed to the pipeline by using `VAR_API_URL` when calling this pipe.  The pipe will get all variables beginning with `VAR_`, strip the `VAR_` and pass it along to the next pipeline.

#### Secure Variables
To specify a secure variable to be picked up and passed to the pipeline just prefix your variable with SEC_VAR_.  For example the secure variable `API_KEY` can be passed to the pipeline by using `SEC_VAR_API_KEY` when calling this pipe.  The pipe will get all variables beginning with `SEC_VAR_`, strip the `SEC_VAR_` and pass it along to the next pipeline as a secure variable.
