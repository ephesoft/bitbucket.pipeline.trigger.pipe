BeforeAll {
  Import-Module './pipe/invoke-pipeline.psm1' -Force
  Import-Module './pipe/wait-preexistingpipeline.psm1' -Force
}

Describe 'Invoke-Pipeline' {
  BeforeAll {
    Mock Start-BitbucketPipeline -ModuleName Invoke-Pipeline  {
      $Response = [ordered]@{
        uuid         = ('123')
        build_number = 1
        state        = @{
          name = 'PENDING'
        }
      }
      return $Response
    }
    Mock Write-BBBlue -ModuleName Invoke-Pipeline  {}
    Mock Write-BBWarning -ModuleName Invoke-Pipeline  {}
    Mock Wait-BitbucketPipeline -ModuleName Invoke-Pipeline  {}
    Mock Wait-PreexistingPipeline -ModuleName Invoke-Pipeline  {}

    $Workspace = 'W'
    $Repo = 'R'
    $Branch = 'B'


  }

  Context 'Normal Trigger' {
    It 'Starts the pipeline' {
      Invoke-Pipeline -Workspace $Workspace -Repo $Repo -Branch $Branch | Out-Null
      Assert-MockCalled Start-BitbucketPipeline -ModuleName Invoke-Pipeline -Exactly 1 -ParameterFilter {
        $RepoSlug -eq $Repo
      }
    }
    It 'Waits for the correct pipeline to finish' {
      Invoke-Pipeline -Workspace $Workspace -Repo $Repo -Branch $Branch | Out-Null
      Assert-MockCalled Wait-BitbucketPipeline -ModuleName Invoke-Pipeline -Exactly 1 -ParameterFilter {
        $PipelineUUID -eq '123'
      }
    }
    It 'Does not retry' {
      Invoke-Pipeline -Workspace $Workspace -Repo $Repo -Branch $Branch | Out-Null
      Assert-MockCalled Wait-PreexistingPipeline -ModuleName Invoke-Pipeline -Exactly 0
    }
  }

  Context 'Existing Pipline Running' {
    BeforeAll {

      Mock Wait-BitbucketPipeline -ModuleName Invoke-Pipeline {
        Throw 'halted'
      }
    }
    It 'Waits for the pre-existing pipeline to finish' {
      Invoke-Pipeline -Workspace $Workspace -Repo $Repo -Branch $Branch -restartLimit 1 | Out-Null
      Assert-MockCalled Wait-PreexistingPipeline -ModuleName Invoke-Pipeline -ParameterFilter {
        $PipelineUUID -eq '123'
      }
    }
    It 'Tries to start the pipeline an appropriate number of times' {
      Invoke-Pipeline -Workspace $Workspace -Repo $Repo -Branch $Branch -restartLimit 1 | Out-Null
      Assert-MockCalled Start-BitbucketPipeline -ModuleName Invoke-Pipeline -Exactly 2
    }
    It 'Does not retry when restartLimit set to 0' {
      Invoke-Pipeline -Workspace $Workspace -Repo $Repo -Branch $Branch -restartLimit 0 | Out-Null
      Assert-MockCalled Start-BitbucketPipeline -ModuleName Invoke-Pipeline -Exactly 1
    }
  }
}