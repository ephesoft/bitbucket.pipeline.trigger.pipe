Describe 'ScriptAnalyzer' {
  Context 'Validating ScriptAnalyzer installation' {
    It 'Checking Invoke-ScriptAnalyzer exists.' {
      { Get-Command Invoke-ScriptAnalyzer -ErrorAction Stop } | Should -Not -Throw
    }
  }
}

Describe 'ScriptAnalyzer issues found' {

  $ExcludedFiles = @('test')
  $ScriptAnalyzerSettings = @{

    <#
      PSAvoidUsingWriteHost: Write-Host is used for color-coded output in the Bitbucket pipeline interface only
      PSAvoidUsingConvertToSecureStringWithPlainText: The plain text credential is pulled directly from a secure Bitbucket variable
      UseSupportsShouldProcess: Confirm param required for confirming a pipeline has run successfully not for replacing SupportsShouldProcess
    #>
    ExcludeRules = @('PSAvoidUsingWriteHost','PSAvoidUsingConvertToSecureStringWithPlainText','PSUseSupportsShouldProcess')
  }
  $results = Get-ChildItem .\* -Exclude $ExcludedFiles  | Invoke-ScriptAnalyzer -Settings $ScriptAnalyzerSettings
  $scripts = $results.ScriptName | Get-Unique

  Context 'Checking results' {
    It 'Should have no issues' {
      $results.count | Should -Be 0
    }
  }

  foreach ($script in $scripts) {
    Context $script {
      $issues = $results | Where-Object { $_.ScriptName -eq $script }

      foreach ($issue in $issues) {
        It "On line: $($issue.Line) - $($issue.Message)" {
          $true | Should -Be $False
        }
      }
    }
  }
}