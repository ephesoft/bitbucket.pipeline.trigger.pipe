FROM mcr.microsoft.com/powershell
RUN pwsh -command "Install-Module atlassian.bitbucket -Repository PSGallery -Force"
SHELL ["pwsh"]
COPY pipe /usr/bin/
ENTRYPOINT ["pwsh", "/usr/bin/pipe.ps1" ]
