# Install required modules
Install-Module PSScriptAnalyzer -Force
Install-Module Pester -Force
Install-Module Atlassian.Bitbucket -Force

# Import required modules
Import-Module PSScriptAnalyzer
Import-Module Pester
Import-Module Atlassian.Bitbucket

# Configure Pester 5 settings for optimal pipeline output
$PesterConfig = [PesterConfiguration]::Default
$PesterConfig.Output.Verbosity = 'Detailed'
$PesterConfig.Run.Path = '.\*'
$PesterConfig.Run.Exit = $true
Invoke-Pester -Configuration $PesterConfig